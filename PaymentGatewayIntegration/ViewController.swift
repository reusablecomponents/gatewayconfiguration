//
//  ViewController.swift
//  PaymentGatewayIntegration
//
//  Created by Sakshi on 09/05/18.
//  Copyright © 2018 Signity Software Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Stripe

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func startStripePaymentGatewayIntegration(_ sender: UIButton) {
    
        let config = STPPaymentConfiguration.shared()
        config.requiredBillingAddressFields = .none
        //Here is your token
        paymentCompletionHandler = {token in
            let stripeToken = token.tokenId
            print(stripeToken)
            
            let alert = UIAlertController(title: "Here is your stripe token", message: stripeToken, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert, animated: true)

        }
        paymentCancelHandler = {
                //Handle cancel action here
        
        }
        let viewController = STPAddCardViewController(configuration: config, theme: .default())
        viewController.delegate = self
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.stp_theme = .default()
        present(navigationController, animated: true, completion: nil)
    }
}
