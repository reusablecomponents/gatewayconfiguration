//
//  StripeExtension.swift
//  TowAssist
//
//  Created by Nivedita on 06/03/18.
//  Copyright © 2018 Anisha. All rights reserved.
//

import Stripe
// swiftlint:disable line_length
var paymentCompletionHandler: ((STPToken) -> Void)?
var paymentCancelHandler : (() -> Void)?
extension UIViewController: STPAddCardViewControllerDelegate {
    // MARK: STPAddCardViewControllerDelegate
    public func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true, completion: nil)
        paymentCancelHandler!()
    }
    public func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        // Once token genrated call API for payment
        paymentCompletionHandler!(token)
        dismiss(animated: true, completion: nil)
    }
}
